<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 14.07.16
 * Time: 15:55
 */

namespace ORM;


use INur\Framework\Loader;

class ORM
{
    public static function factory($data){
        $loader = new Loader();
        $loader->load(APP_SRC.'Models/'.ucfirst($data).'.php');
        $class = APP_NAMESPACE.'Models\\'.ucfirst($data);
        /** @var Model $object */
        $object = new $class();
        return $object;
    }

}