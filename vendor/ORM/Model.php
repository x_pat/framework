<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 14.07.16
 * Time: 16:20
 */

namespace ORM;


abstract class Model
{
    /**
     * @var DB
     */
    protected $db;

    /**
     * @var null
     */
    protected $tableName = null;

    /**
     * @var array
     */
    protected $fields = array();

    /**
     * @param array $array
     * @return array
     */
    public function findAll($array = array()){

        $fields = '*';
        if(count($array)>0){
            $fields = implode(',',$array);
        }
        $dbh = $this->db->getDBH();
        $sdh = $dbh->query("SELECT {$fields} FROM ".$this->tableName);
        $sdh->setFetchMode(\PDO::FETCH_ASSOC);

        $data = array();
        while($object = $sdh->fetchObject()){
            $data[] = $object;
        }
        return $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id){
        $dbh = $this->db->getDBH();
        $sdh = $dbh->prepare("SELECT * FROM ".$this->tableName.' WHERE id = :id');
        $sdh->bindParam(':id',$id);
        $sdh->execute();
        $sdh->setFetchMode(\PDO::FETCH_ASSOC);
        return $object = $sdh->fetchObject();
    }

    /**
     * @param $field
     * @param $value
     * @return $this
     */
    public function set($field,$value){
        $this->fields[$field] = $value;
        return $this;
    }

    public function update($id){

        if(is_array($this->fields) && count($this->fields) > 0){
            $str = '';
            foreach($this->fields as $key => $field){
                $str .= ', '.$key.' = :'.$key;
            }
            $dbh = $this->db->getDBH();
            $sdh = $dbh->prepare("UPDATE  ".$this->tableName." SET ".substr($str,1)." WHERE id = :id; ");
            $sdh->bindParam(':id',$id);

            foreach($this->fields as $key => $field){
                $sdh->bindValue(':'.$key, $field);
            }
            $sdh->execute();
        }
    }

    public function create(){

        if(is_array($this->fields) && count($this->fields) > 0){
            $strField = '';
            $strVal = '';
            foreach($this->fields as $key => $field){
                $strField .= ', '.$key;
                $strVal .= ', '.':'.$key;
            }
            $dbh = $this->db->getDBH();
            $sdh = $dbh->prepare("INSERT INTO  ".$this->tableName."(".substr($strField,1).") VALUES(".substr($strVal,1)."); ");

            foreach($this->fields as $key => $field){
                $sdh->bindValue(':'.$key, $field);
            }
            $sdh->execute();
        }
    }

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = new DB();
    }

}