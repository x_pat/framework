<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 14.07.16
 * Time: 15:32
 */

namespace ORM;


use config\DataBase;

class DB
{
    public function getDBH(){
        $config = DataBase::$config;
        $link = new \PDO("mysql:host=".$config['host'].";dbname=".$config['db'], $config["user"], $config["password"]);
        $link->exec('SET NAMES '.$config['charset']);
        return $link;
    }
}