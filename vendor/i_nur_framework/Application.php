<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 13.07.16
 * Time: 12:39
 */



namespace INur\Framework;


use config\Route;

class Application
{
    /**
     * @var array
     */
    private $route = null;

    /**
     * @var null|string
     */
    private $routeName = null;

    /**
     * @var string
     */
    private $url;

    /**
     * @var
     */
    private $action;


    /**
     * @return string
     */
    private function getDirectory(){
        if(isset($this->route['directory'])){
            return $this->route['directory'].'/';
        }
        else{
            return '';
        }
    }


    /**
     * @param $param
     * @return mixed
     */
    private function getRouteParameter($param){
        if(isset($this->route[$param])){
            return $this->route[$param];
        }
        else{
            new Exception('"'.$param.'" is not defined in route "'.$this->routeName.'" config/Route');
        }
    }

    /**
     * @return null
     */
    public function getClass(){
        return APP_NAMESPACE.'Controllers\\'.str_replace('/','\\',$this->getDirectory()).ucfirst($this->getRouteParameter('controller')).'Controller';
    }

    /**
     * @return null
     */
    public function getFile(){

        return APP_SRC.'Controllers/'.$this->getDirectory().ucfirst($this->getRouteParameter('controller')).'Controller.php';
    }

    /**
     * getAction
     */
    public function run(){
        echo $this->action;
    }


    /**
     * Application constructor.
     */
    public function __construct()
    {
        $request_uri = parse_url(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        $url = strtolower(urldecode($request_uri['path']));
        $data = explode('/',$url);
        $route = new Route();
        $this->url = $url;

        // Если это не корневая директория сайта
        if(count($data) > 2){
            //Если роут простой типа 'index'
            if(isset($route->rotes[$data[1]])){
                $this->route = $route->rotes[$data[1]];
                $this->routeName = $data[1];
            }
            // Если сложный типа 'admin/index'
            else{
                $result = $data[1];
                for($i = 2; $i < count($data); $i++){

                    $result.= '/'.$data[$i];
                    if(isset($route->rotes[$result])){
                        $this->route = $route->rotes[$result];
                        $this->routeName = $result;
                        break;
                    }
                }
            }
        }
        // Если корневая директория сайта
        else{
            if(isset($route->rotes['index'])){
                $this->route = $route->rotes['index'];
                $this->routeName = 'index';
            }
            else{
                new Exception('Undefined "default" route in config/Route');
            }

        }
        // Если роут не найден
        if($this->route == null){
            if(isset($route->rotes['not_found'])){
                $this->route = $route->rotes['not_found'];
                $this->routeName = 'not_found';
            }
            else{
                new Exception('Undefined "not_found" route in config/Route');
            }

        }

        $this->setActionAndParams();
    }

    /**
     *
     */
    private function setActionAndParams(){

        // является ли переданный параметр экшеном
        $isAction = false;

        $loader = new Loader();
        $loader->load($this->getFile());

        $class = $this->getClass();
        $controller = new $class();
        // Получаем массив параметров
        $data = explode('/',str_replace('/'.$this->routeName,'',$this->url));
        // Удаляем пустые ячейки и сбрасываем ключи массива
        $data = array_values(array_filter($data));

        // Если есть параметр и есть такой экшн
        if(isset($data[0]) && method_exists($controller,$data[0].'Action')){
            $action = $data[0].'Action';
            $isAction = true;
        }// Иначае дефолтный экшн
        else{
            $action = $this->getRouteParameter('default_action').'Action';
        }
        // Присвоение переданных через url параметров если таковые предусмотрены в настройках роута
        if(isset($this->route['parameters']) && is_array($this->route['parameters'])){
            $index = $isAction == true?1:0;
            $parameters = array();
            foreach($this->route['parameters'] as $parameter){
                if(isset($data[$index])){
                    $parameters[$parameter] = trim(strip_tags($data[$index]));
                }
                $index++;
            }
            $controller->setParameters($parameters);
        }
        // Найден ли экшн?
        if(method_exists($controller,$action)){
            $this->action = $controller->$action();
        }else{
            new Exception('"'.$action.'" action not found in '.$this->getClass());
        }
    }

}