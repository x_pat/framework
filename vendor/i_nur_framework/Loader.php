<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 13.07.16
 * Time: 10:19
 */
namespace INur\Framework;

require 'config/Route.php';
require_once 'config/DataBase.php';
require 'vendor/i_nur_framework/Application.php';
require 'vendor/i_nur_framework/Exception.php';
require 'vendor/i_nur_framework/Controller.php';
require_once 'vendor/ORM/DB.php';
require_once 'vendor/ORM/Model.php';
require_once 'vendor/ORM/ORM.php';
require_once 'vendor/Twig/lib/Twig/Autoloader.php';

\Twig_Autoloader::register();


class Loader{
    public static function load($class){
        if(is_file($class)){
            require_once "$class";
        }else{
             new Exception("Файл {$class} не найден");
        }

    }
}
?>