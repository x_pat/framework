<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 13.07.16
 * Time: 9:30
 */

namespace INur\Framework;


abstract class Controller
{
    /**
     * @var
     */
    protected $parameters;

    /**
     * @return mixed
     */
    protected function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param $index
     * @return null
     */
    protected function getParameter($index){
        if(isset($this->parameters[$index])){
            return $this->parameters[$index];
        }else{
            return null;
        }
    }

    /**
     * @return mixed
     */
    protected function getRequest(){
        return $_REQUEST;
    }

    /**
     * @param $view
     * @param $parameters
     * @return string
     */
    public function renderTwig($view, $parameters = array()){
        if(is_file( APP_SRC.'views/'.$view)){
            $loader = new \Twig_Loader_Filesystem(array(
                $view => APP_SRC.'views',
            ));
            $twig = new \Twig_Environment($loader);

            return $twig->render($view, $parameters);
        }else{
            new Exception(APP_SRC.'views/'.$view.' Not Found!');
        }

    }

}