<?php

namespace Src\Controllers;

use INur\Framework\Controller;
use ORM\ORM;

class IndexController extends Controller{

    public function indexAction(){

       // $user = ORM::factory('users')->find(1);
        $users = ORM::factory('users')->findAll();
//        ORM::factory('users')
//            ->set('f_name','Mike')
//            ->set('email','mike@mail.ru')
//            ->create();

      return $this->renderTwig('Index/index.html.twig',array(
          'users' => $users
      ));

    }

    public function testAction(){

        return $this->renderTwig('Index/test.html.twig',array(
            'message' => 'This Test Page'
        ));
    }

}

?>