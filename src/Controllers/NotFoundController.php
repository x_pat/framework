<?php

namespace Src\Controllers;

use INur\Framework\Controller;

class NotFoundController extends Controller{

    public function not_foundAction(){
        return $this->renderTwig('NotFound/not_found.html.twig',array(
            'message' => 'Страница не найдена'
        ));
    }
}

?>