<?php
/**
 * Created by PhpStorm.
 * User: ali
 * Date: 13.07.16
 * Time: 11:48
 */

namespace config;


class Route
{
    public $rotes = array(

        // обязательный роут Default
        'index' => array(
            'controller' => 'Index', // обязательный параметр
            'default_action' => 'index', // обязательный параметр
            'parameters' => array('name'), // необязательный параметр
          //  'directory' => 'Admin' // необязательный параметр
        ),
        // обязательный роут
        'not_found' => array(
            'controller' => 'NotFound',
            'default_action' => 'not_found',
        ),
        'admin/index' => array(
            'controller' => 'Index',
            'default_action' => 'index',
            'directory' => 'Admin'
        ),

    );

}